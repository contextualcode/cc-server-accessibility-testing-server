# Accessibility Testing Server
Internal Contextual Code Accessibility Testing Server. It suppose to be installed on `Solr` server at `/opt/cc-server-selenium-server` path.
## Installation
```
ssh-cc-solr
cd /opt
git clone git@gitlab.com:contextualcode/cc-server-accessibility-testing-server.git
cd ./cc-server-accessibility-testing-server
git submodule update --init --recursive
cd ./AATT
sudo npm install
sudo npm install forever-monitor
sudo npm install forever -g
```
## Usage
```
ssh-cc-solr
cd /opt/cc-server-accessibility-testing-server && ./restart.sh
```
