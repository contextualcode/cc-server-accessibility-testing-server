#!/usr/bin/env bash
cd ./AATT
TMP=$(forever list | grep aatt | wc -l)
if [ $TMP -eq 0 ] ; then
   http_port=3000 forever -s start --minUptime 1000 --spinSleepTime 1000 --append --uid aatt app.js
fi
